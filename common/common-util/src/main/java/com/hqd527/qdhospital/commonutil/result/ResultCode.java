package com.hqd527.qdhospital.commonutil.result;

/**
 * @author 太优秀
 * @date 2021年09月12日16:55
 * @apiNote
 */
public interface ResultCode {

    /**成功状态码**/
    public static Integer SUCCESS = 200;

    /**失败状态码**/
    public static Integer ERROR = 201;

}
