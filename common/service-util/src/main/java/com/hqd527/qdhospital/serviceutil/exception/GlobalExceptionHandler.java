package com.hqd527.qdhospital.serviceutil.exception;

import com.hqd527.qdhospital.commonutil.result.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author 太优秀
 * @date 2021年09月12日22:53
 * @apiNote
 */

/**
 * 全局异常处理：只要有异常就触发这个异常处理类
 */

// @ControllerAdvice 这个注解就是 Spring Aop 的应用
@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(QDHospitalException.class)
    @ResponseBody
    public Result error(QDHospitalException e) {

        // 使用自定义输出异常信息工具类：在日志中也打印出【完整】的错误日志信息，方便排错
        log.error(ExceptionUtils.getMessage(e));

        return Result.error().message(e.getMessage()).code(e.getCode());

    }

}
