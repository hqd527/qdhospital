package com.hqd527.qdhospital.serviceutil.exception;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * @author 太优秀
 * @date 2021年09月13日11:01
 * @apiNote
 */

/**
 * 【自定义异常处理工具类】
 *      为了保证日志中的错误堆栈信息能够被输出，
 *      也就是在日志文件中也有 控制台输出的那些完整异常信息
 */

public class ExceptionUtils {

    public static String getMessage(Exception e) {
        StringWriter sw = null;
        PrintWriter pw = null;
        try {
            sw = new StringWriter();
            pw = new PrintWriter(sw);
            // 将出错的栈信息输出到printWriter中
            e.printStackTrace(pw);
            pw.flush();
            sw.flush();
        } finally {
            if (sw != null) {
                try {
                    sw.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            if (pw != null) {
                pw.close();
            }
        }
        return sw.toString();
    }

}
