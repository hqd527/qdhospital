package com.hqd527.qdhospital.serviceutil.config;

import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger2配置信息
 */
@Configuration
@EnableSwagger2
public class Swagger2Config {

    /**
     * 后台管理员swagger接口测试页面
     * @return
     */
    @Bean
    public Docket adminApiConfig(){
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("adminApi")
                .apiInfo(adminApiInfo())
                .select()
                .paths(Predicates.and(PathSelectors.regex("/admin/.*")))
                .build();

    }


    /**
     * 前台用户swagger接口测试页面
     * @return
     */
    @Bean
    public Docket frontApiConfig(){
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("frontApi")
                .apiInfo(frontApiInfo())
                .select()
                .paths(Predicates.and(PathSelectors.regex("/front/.*")))
                .build();

    }


    private ApiInfo adminApiInfo(){
        return new ApiInfoBuilder()
                .title("《QDHOSPITAL》后端接口说明")
                .description("此文档详细说明了《QDHOSPITAL》项目后台管理系统接口规范....")
                .version("v 2.0.1")
                .contact(new Contact("史上最菜大学生QD", "http://www.hqd527.work", "2097791505@qq.com"))
                .build();
    }

    private ApiInfo frontApiInfo(){
        return new ApiInfoBuilder()
                .title("《QDHOSPITAL》前台接口说明")
                .description("此文档详细说明了《QDHOSPITAL》项目前台系统接口规范....")
                .version("v 2.0.1")
                .contact(new Contact("史上最菜大学生QD", "http://www.hqd527.work", "2097791505@qq.com"))
                .build();
    }

}
