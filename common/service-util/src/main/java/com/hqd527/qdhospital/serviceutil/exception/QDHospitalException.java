package com.hqd527.qdhospital.serviceutil.exception;


import com.hqd527.qdhospital.commonutil.result.ResultCodeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 太优秀
 * @date 2021年09月13日10:49
 * @apiNote 【自定义异常类】
 */


@Data
@NoArgsConstructor
@AllArgsConstructor
public class QDHospitalException extends RuntimeException{

    @ApiModelProperty(value = "状态码")
    private Integer code;

    /**
     * 接收程序员手动输入的状态码和消息
     * @param code
     * @param message
     */
    public QDHospitalException(String message, Integer code) {
        super(message);
        this.code = code;
    }

    /**
     * 接收自定义好的枚举类型
     * @param resultCodeEnum
     */
    public QDHospitalException(ResultCodeEnum resultCodeEnum) {
        super(resultCodeEnum.getMessage());
        this.code = resultCodeEnum.getCode();
    }

    @Override
    public String toString() {
        return "QDException{" +
                "code=" + code +
                ", message=" + this.getMessage() +
                '}';
    }


}
