package com.hqd527.qdhospital.servicehosp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author 太优秀
 * @date 2022/3/28 20:13
 * @description
 */

@SpringBootApplication
//@MapperScan("com.hqd527.qdhospital.servicehosp.mapper") 在MybatisPlusConfig中定义了
@ComponentScan(basePackages = "com.hqd527.qdhospital")
public class HospServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(HospServiceApplication.class,args);
    }
}
