package com.hqd527.qdhospital.servicehosp.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hqd527.qdhospital.commonutil.model.hosp.HospitalSet;
import com.hqd527.qdhospital.servicehosp.mapper.HospitalSetMapper;
import com.hqd527.qdhospital.servicehosp.service.HospitalSetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author 太优秀
 * @date 2022/3/29 0:38
 * @description
 */

@Service
public class HospitalSetServiceImpl extends ServiceImpl<HospitalSetMapper, HospitalSet> implements HospitalSetService {

    @Autowired
    private HospitalSetMapper hospitalSetMapper;

    /**
     * 据关键字模糊查询医院名称列表
     * @param keyword
     * @return
     */
    @Override
    public List<Map<String, Object>> getNameListByKeyword(String keyword) {
        QueryWrapper<HospitalSet> hospitalSetQueryWrapper = new QueryWrapper<>();
        hospitalSetQueryWrapper.select("hosname").like("hosname",keyword);
        List<Map<String, Object>> hosNameList = hospitalSetMapper.selectMaps(hospitalSetQueryWrapper);
        return hosNameList;
    }

    /**
     * 根据医院名称查询出医院编号
     * @param hosName
     * @return
     */
    @Override
    public List<Map<String, Object>> getHosCodeByHosName(String hosName) {
        QueryWrapper<HospitalSet> hospitalSetQueryWrapper = new QueryWrapper<>();
        hospitalSetQueryWrapper.select("hoscode").eq("hosname",hosName);
        List<Map<String, Object>> hosCodeList = hospitalSetMapper.selectMaps(hospitalSetQueryWrapper);
        return hosCodeList;
    }

}
