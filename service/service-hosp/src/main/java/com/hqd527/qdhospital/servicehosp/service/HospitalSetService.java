package com.hqd527.qdhospital.servicehosp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hqd527.qdhospital.commonutil.model.hosp.HospitalSet;

import java.util.List;
import java.util.Map;

/**
 * @author 太优秀
 * @date 2022/3/29 0:38
 * @description
 */
public interface HospitalSetService extends IService<HospitalSet> {
    // 根据关键字模糊查询医院名称列表
    public List<Map<String, Object>> getNameListByKeyword(String keyword);

    //根据医院名称查询出医院编号
    public List<Map<String, Object>> getHosCodeByHosName(String hosName);
}
