package com.hqd527.qdhospital.servicehosp.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hqd527.qdhospital.commonutil.model.hosp.HospitalSet;
import com.hqd527.qdhospital.commonutil.result.Result;
import com.hqd527.qdhospital.commonutil.vo.hosp.HospitalSetQueryVo;
import com.hqd527.qdhospital.servicehosp.service.HospitalSetService;
import com.hqd527.qdhospital.serviceutil.utils.MD5;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Random;

;

/**
 * @author 太优秀
 * @date 2022/3/29 0:41
 * @description
 */

@Api(tags = "医院设置管理")
@RestController
@CrossOrigin
@RequestMapping("/admin/servicehosp/hospitalSet")
public class HospitalSetController {

    @Autowired
    private HospitalSetService hospitalSetService;

    //【1】查询医院设置表所有信息
    @ApiOperation(value = "获取所有医院设置")
    @GetMapping("/findAllHospitalSet")
    public Result findAllHospitalSet() {
        //调用service的方法
        List<HospitalSet> hospitalSetList = hospitalSetService.list();
        return Result.ok().data("hospitalSetList",hospitalSetList);
    }

    //【2】根据id逻辑删除医院设置
    @ApiOperation(value = "根据id逻辑删除医院设置")
    @DeleteMapping("/removeHospById/{id}")
    public Result removeHospById(@PathVariable("id") Long id){
        boolean flag = hospitalSetService.removeById(id);

        if(flag){
            return Result.ok();
        }else{
            return Result.error();
        }
    }

    //【3】分页查询医院设置
    //【注意：使用了@RequestBody注解，那么请求方式就得换成@PostMapping，否则获取不到数据】
    @ApiOperation("分页查询医院设置")
    @PostMapping("/findPageHospSet/{current}/{limit}")
    public Result findPageHospSet(@PathVariable("current") @ApiParam(value = "当前页") Long current,
                                  @PathVariable("limit") @ApiParam(value = "每页显示记录数") Long limit,
                                  @RequestBody(required = false) HospitalSetQueryVo hospitalSetQueryVo) {
        // 创建page对象，传递当前页 和 每页记录数
        Page<HospitalSet> pageParam = new Page<>(current, limit);
        // 构建条件
        QueryWrapper<HospitalSet> hospitalSetQueryWrapper = new QueryWrapper<>();
        hospitalSetQueryWrapper.orderByDesc("update_time");

        if(!StringUtils.isEmpty(hospitalSetQueryVo.getHosname())){      // 医院名称
            hospitalSetQueryWrapper.like("hosname",hospitalSetQueryVo.getHosname());
        }
        if(!StringUtils.isEmpty(hospitalSetQueryVo.getHoscode())){      // 医院编号
            hospitalSetQueryWrapper.eq("hoscode",hospitalSetQueryVo.getHoscode());
        }
        // 分页查询医院设置
        Page<HospitalSet> pageHospitalSetList = hospitalSetService.page(pageParam, hospitalSetQueryWrapper);

        return Result.ok().data("pageHospitalSetList",pageHospitalSetList);
    }


    //【4】添加医院设置
    @ApiOperation(value = "添加医院设置")
    @PostMapping("/saveHospSet")
    public Result saveHospSet(@RequestBody HospitalSet hospitalSet){
        // 设置状态
        hospitalSet.setStatus(1);
        // 设置签名密钥
        String signKey = MD5.encrypt(System.currentTimeMillis() + "" + new Random().nextInt());
        hospitalSet.setSignKey(signKey);
        // 保存医院设置
        boolean flag = hospitalSetService.save(hospitalSet);

        if(flag){
            return Result.ok();
        }else{
            return Result.error();
        }
    }


    //【5】根据id获取医院设置
    @ApiOperation(value = "根据id获取医院设置")
    @GetMapping("/getHospSetById/{id}")
    public Result getHospSetById(@PathVariable("id") Long id){
        HospitalSet hospitalSet = hospitalSetService.getById(id);
        return Result.ok().data("hospitalSet",hospitalSet);
    }


    //【6】修改医院设置
    @ApiOperation(value = "修改医院设置")
    @PostMapping("/updateHospSet")
    public Result updateHospSet(@RequestBody HospitalSet hospitalSet){
        boolean flag = hospitalSetService.updateById(hospitalSet);

        if(flag){
            return Result.ok();
        }else{
            return Result.error();
        }
    }


    //【7】批量删除医院设置
    @ApiOperation(value = "批量删除医院设置")
    @DeleteMapping("/batchRemoveByIdList")
    public Result batchRemoveHospSetByIdList(@RequestBody List<Long> idList){
        boolean flag = hospitalSetService.removeByIds(idList);

        if(flag){
            return Result.ok();
        }else{
            return Result.error();
        }
    }


    //【8】医院设置： 锁定/解锁
    @ApiOperation(value = "医院设置是否锁定")
    @PostMapping("/lockHospSet/{id}/{status}")
    public Result lockHospSetById(@PathVariable("id") Long id,
                                  @PathVariable("status") Integer status){
        // 根据id获取医院设置
        HospitalSet hospitalSet = hospitalSetService.getById(id);
        //设置锁定状态
        hospitalSet.setStatus(status);
        // 更新医院设置
        boolean flag = hospitalSetService.updateById(hospitalSet);

        if(flag){
            return Result.ok();
        }else{
            return Result.error();
        }

    }


    //【9】发送签名密钥
    @ApiOperation(value = "发送签名秘钥")
    @PostMapping("/sendKey/{id}")
    public Result sendKey(@PathVariable("id") Long id){
        // 根据id获取医院设置
        HospitalSet hospitalSet = hospitalSetService.getById(id);
        // 获取签名密钥
        String signKey = hospitalSet.getSignKey();
        // 获取医院编号
        String hoscode = hospitalSet.getHoscode();
        // 【短信发送 TODO】

        return Result.ok();

    }

    //【10】根据医院名称模糊查询
    @ApiOperation(value = "根据医院名称模糊查询")
    @GetMapping("/getNameListByKeyword/{keyword}")
    public Result getNameListByKeyword(@PathVariable("keyword") String keyword){
        List<Map<String, Object>> hosNameList = hospitalSetService.getNameListByKeyword(keyword);
        return Result.ok().data("hosNameList",hosNameList);
    }

    //【11】根据医院名称查询出医院编号
    @ApiOperation(value = "根据医院名称查询出医院编号")
    @GetMapping("/getHosCodeByHosName/{hosName}")
    public Result getHosCodeByHosName(@PathVariable("hosName") String hosName){
        List<Map<String, Object>> hosCodeList = hospitalSetService.getHosCodeByHosName(hosName);
        return Result.ok().data("hosCodeList",hosCodeList);
    }


}
