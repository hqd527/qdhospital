package com.hqd527.qdhospital.servicehosp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hqd527.qdhospital.commonutil.model.hosp.HospitalSet;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 太优秀
 * @date 2022/3/28 20:44
 * @description
 */

@Mapper
public interface HospitalSetMapper extends BaseMapper<HospitalSet> {

}
